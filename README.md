# ELO Client Starter

## Disclaimer

This tool was developed by Phillip Partsch. The author is not an employee of ELO Digital Office GmbH. ELO Digital Office
GmbH is
not responsible for any kind of support. If you find some bugs or want some features to be implemented please contact
the author directly.

This tool is licensed under the APACHE LICENSE, VERSION 2.0. See LICENCE File for more details
or visit https://www.apache.org/licenses/LICENSE-2.0

## Usage

When you start the tool for the first time you are promoted with a license Windows.
You have to accept the licence to continue.

During the first startup 2 folders are created. One folder is called "jc" the other folder is called "java".
On every startup these 2 folders are searched for subfolder, if a subfolder is found it is displayed in the gui.
You can select your desired client version to start the client. You can also select the archive which should
be selected in the dropdown of the Java Client.

### jc folder

You have to copy an installed ELO Java Client into this folder. Typically, you will find the installation of you client
under *C:\Program Files\ELO Java Client*. You should rename the copy of the folder according to the version,
e.g. 20.10.000.246. To save some disk space, you can delete these folders in your copy

* /Ocr/*
* /EloPrintArchive*

### jdk folder

You have to place a custom jdk which should be used to run the client. Any jdk with the correct version should work.
You can also use the jre/jdk, which is shipped with the client. You find it unser *C:\Program Files\ELO Java Client\jre*

#### Which JDK Should I use?

You should always use the jdk which is shipped with the java client. In some cases you would want another jdk to be
used, e.g. if you need to test the client against a newer jdk.

By default (if you don't select the Custom JDK button), the jdk placed in the /jre folder in your client is used.

### Archives

The list is populated with the registry entries of your Java Client installation.
Only the first 100 archive entries are selected.

### SSO

If you check the SSO checkbox a registry key is written to
*HKCU\Software\JavaSoft\Prefs\Elo Digital Office\eloenterprise\issinglesignon*
this enables SSO login turned on/off for different archived with zero effort.
Remember that, since the value is persisted in the registry, this will also affect the default client startup.

### Debug

This will start the Java Client with debug parameters (*-Xdebug -Xrunjdwp:
transport=dt_socket,server=y,suspend=n,address=5005*)
this is only needed if you develop Java libraries directory for the client (.jar files placed in the Java Client
Scripting Base)
AND if you like to debug them.

## Installation

The tool doesn't require an installation. Just run the executable.

## Build / Develop

### Requirements

* IntelliJ Idea (recommended)
* WIX Toolset (https://wixtoolset.org/) to build executable.
* JDK 17+

### Build executable

* ./gradlew exe

### Build jar

* ./gradlew jar

## Tested Clients

* 20.06.001.165
* 20.10.000.246
* 20.11.000.268
* 20.12.001.309
* 21.04.000