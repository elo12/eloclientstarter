$version = $args[0]

$FolderName = "./build/jpackager"
if (Test-Path $FolderName)
{
    Remove-Item $FolderName -Force -Recurse
}

jpackage.exe `
    --type app-image `
    --name ELOClientStarter `
    --copyright 'Phillip Partsch <info@phillipppartsch.de>' `
    --description 'Starts ELO Java Clients of different Versions' `
    --vendor 'Phillip Partsch' `
    --app-version $version `
    --input build/libs `
    --dest build/jpackager `
    `
    --main-jar eloclientstarter.jar `
    --main-class 'de.pp.eloclientstarter.Main' `
    `
    --icon src/jpackager/wix/ApplicationIcon.ico `
    --resource-dir src/jpackager/wix `
    --verbose

