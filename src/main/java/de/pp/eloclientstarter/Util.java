package de.pp.eloclientstarter;

import javafx.scene.control.Tooltip;
import javafx.util.Duration;

import javax.tools.Tool;

public abstract class Util {

    public static String escapeBatch(String string) {
        string = string.replaceAll("%", "%%");
        return string;
    }

    public static boolean isInteger(String string) {
        try {
            Integer.valueOf(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static Tooltip makeTooltip(String content) {
        Tooltip tooltip = new Tooltip(content);
        tooltip.setShowDelay(new Duration(250));
        return tooltip;
    }
}
