package de.pp.eloclientstarter;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProcessDirectories {

    private static final String CLIENT_DIR = "jc";
    private static final String JAVA_DIR = "jdk";
    
    private Path getOrCreateDirectory(String directory) throws IOException {
        Path path = Paths.get(directory);
        if (Files.exists(path)) {
            return path;
        }
        return Files.createDirectories(path);
    }

    private Set<Path> listSubfolder(Path path) throws IOException {
        try (Stream<Path> stream = Files.list(path)) {
            return stream
                    .filter(Files::isDirectory)
                    .collect(Collectors.toSet());
        }
    }

    private Set<Path> findFolder(String baseDir) {
        try {
            var directory = getOrCreateDirectory(baseDir);
            return listSubfolder(directory);
        } catch (IOException e) {
            MainFrame.logError("findFolder=" + baseDir, e);
        }
        return new HashSet<>();
    }

    public List<JavaClientFolder> findClients() {
        var subfolder = findFolder(CLIENT_DIR);

        return subfolder.stream()
                .map(path -> new JavaClientFolder(path.getFileName().toString(), path))
                .sorted()
                .toList();
    }

    public List<JdkFolder> findJdks() {
        var subfolder = findFolder(JAVA_DIR);

        return subfolder.stream()
                .map(path -> new JdkFolder(path.getFileName().toString(), path))
                .toList();
    }
}
