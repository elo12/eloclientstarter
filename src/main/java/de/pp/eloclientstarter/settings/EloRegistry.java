package de.pp.eloclientstarter.settings;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

public class EloRegistry {

    public static final String NODE = "elo digital office/eloenterprise";

    private static final Preferences preferences = Preferences.userRoot().node(NODE);
    private static final Preferences preferencesSystem = Preferences.systemRoot().node(NODE);

    private static final Map<String, String> archives = new LinkedHashMap<>();

    static {
        archives.putAll(getArchiveNames(preferences));
        archives.putAll(getArchiveNames(preferencesSystem));
    }

    public static String getArchiveUrl(String archiveName) {
        if (archives.containsKey(archiveName)) {
            return archives.get(archiveName);
        }
        return null;
    }

    public static List<String> getArchiveNames() {
        return archives.keySet().stream().sorted().toList();
    }

    private static Map<String, String> getArchiveNames(Preferences preferences) {
        Map<String, String> archives = new LinkedHashMap<>();
        for (int i = 1; i <= 100; ++i) {
            var archiveName = preferences.get("name" + i, "");
            var archiveUrl = preferences.get("archive" + i, "");
            if (archiveName == null || archiveName.isEmpty()) {
                break;
            }

            archives.put(archiveName, archiveUrl);
        }
        return archives;
    }

    public static String getLastSelectedArchive() {
        return preferences.get("lastselected", "");
    }

    public static void setLastSelectedArchive(String name) {
        preferences.put("lastselected", name);
    }

    public static String getSingleSignOn() {
        return preferences.get("issinglesignon", "");
    }

    public static void setSingleSignOn(boolean enabled) {
        preferences.put("issinglesignon", String.valueOf(enabled));
    }

    public static String getLastLogin() {
        return preferences.get("lastlogin", "");
    }

    public static void setLastLogin(String name) {
        preferences.put("lastlogin", name);
    }

}
