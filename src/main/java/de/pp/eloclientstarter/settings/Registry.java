package de.pp.eloclientstarter.settings;

import java.util.prefs.Preferences;

public class Registry {

    public static final String NODE = "de.pp.eloclientstarter";

    private static final Preferences preferences = Preferences.userRoot().node(NODE);

    public static String getLicenceAgreed() {
        return preferences.get("eula", "");
    }

    public static void setLicenceAgreed(String value) {
        preferences.put("eula", value);
    }

    public static String getLastSelectedJavaClient() {
        return preferences.get("lastselectedjavaclient", "");
    }

    public static void setLastSelectedJavaClient(String value) {
        preferences.put("lastselectedjavaclient", value);
    }

    public static String getLastSelectedJDK() {
        return preferences.get("lastselectedjdk", "");
    }

    public static void setLastSelectedJDK(String value) {
        preferences.put("lastselectedjdk", value);
    }

    public static String getLastCustomJDK() {
        return preferences.get("customjdk", "");
    }

    public static void setLastCustomJDK(String value) {
        preferences.put("customjdk", value);
    }

    public static String getLastSelectedDebugPort() {
        return preferences.get("debugport", "5007");
    }

    public static void setLastSelectedDebugPort(String value) {
        preferences.put("debugport", value);
    }

    public static String getLastSelectedMaxRam() {
        return preferences.get("maxram", "1000");
    }

    public static void setLastSelectedMaxRam(String value) {
        preferences.put("maxram", value);
    }
}
