package de.pp.eloclientstarter;

import de.pp.eloclientstarter.settings.EloRegistry;
import de.pp.eloclientstarter.settings.Registry;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ClientRunner {

    public static void startJC(String archive, JdkFolder jdkFolder, boolean customJdk, JavaClientFolder javaClientFolder, boolean debug, String debugPort, String maxRam) throws IOException {
        Registry.setLastSelectedJavaClient(javaClientFolder.name());
        Registry.setLastSelectedDebugPort(debugPort);
        Registry.setLastCustomJDK(String.valueOf(true));
        if (jdkFolder != null) {
            Registry.setLastSelectedJDK(jdkFolder.name());
        }
        EloRegistry.setLastSelectedArchive(archive);

        var command = getJCStartCommand(jdkFolder, customJdk, javaClientFolder, debug, debugPort, maxRam);
        var bat = createBat(command);
        List<String> commands = new ArrayList<>();

        commands.add("cmd");
        commands.add("/C");
        commands.add("start");
        commands.add(bat.toAbsolutePath().toString());
        new ProcessBuilder(commands).start();
    }

    private static Path createBat(String command) throws IOException {
        var path = Paths.get("StartELO.bat");
        Files.writeString(path, command, StandardCharsets.ISO_8859_1);
        return path;
    }

    static String getJCStartCommand(JdkFolder jdkFolder, boolean customJdk, JavaClientFolder javaClientFolder, boolean debug, String debugPort, String maxRam) {
        String javaExecutable = "";
        String libraryPath = Util.escapeBatch(javaClientFolder.getLibraryPath());
        String mainJarPath = Util.escapeBatch(javaClientFolder.getMainJarPath());

        if (jdkFolder != null && customJdk) {
            javaExecutable = Util.escapeBatch(jdkFolder.getJavaExecutable());
        } else {
            javaExecutable = Util.escapeBatch(javaClientFolder.getJavaExecutable());
        }

        StringBuilder builder = new StringBuilder();
        builder.append("chcp 1252\n"); // required for umlauts in filenames

        builder
                .append("\"")
                .append(javaExecutable)
                .append("\"")
                .append(" -Xms200m -Xmx" + maxRam + "m -XX:+ShowCodeDetailsInExceptionMessages ")
                .append(" -Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager ")
                .append(" -Djava.library.path=\"")
                .append(libraryPath)
                .append("\" ")
                .append(" --add-opens=java.desktop/java.awt=ALL-UNNAMED ")
                .append(" --add-opens=java.prefs/java.util.prefs=ALL-UNNAMED ")
                .append(" --add-opens=java.desktop/java.awt.event=ALL-UNNAMED ")
                .append(" --add-opens=java.desktop/sun.awt.datatransfer=ALL-UNNAMED ")
                .append(" --add-opens=java.desktop/sun.awt.windows=ALL-UNNAMED ")
                .append(" --add-opens=java.desktop/javax.swing=ALL-UNNAMED ")
                .append(" --add-opens=java.desktop/javax.swing.event=ALL-UNNAMED ")
                .append(" --add-opens=java.desktop/javax.swing.table=ALL-UNNAMED ")
                .append(" --add-opens=java.base/java.net=ALL-UNNAMED ")
                .append(" --add-opens=java.base/java.lang=ALL-UNNAMED ")
                .append(" --add-opens=java.desktop/sun.font=ALL-UNNAMED ")
                .append(" --add-opens=java.desktop/sun.awt.shell=ALL-UNNAMED ")
                .append(" --add-opens=java.base/sun.util.calendar=ALL-UNNAMED ")
                .append(" --add-opens=java.base/sun.net.www.protocol.https=ALL-UNNAMED ")
                .append(" --add-opens=java.base/sun.net.www.protocol.http=ALL-UNNAMED ")
                .append(" --add-opens=java.base/sun.net.www.http=ALL-UNNAMED ")
                .append("  --add-opens=java.desktop/javax.swing.tree=ALL-UNNAMED ");

        if (debug) {
            builder.append(" -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:" + debugPort + " -jar \"")
                    .append(mainJarPath)
                    .append("\" %*");
        } else {
            builder.append(" -jar \"")
                    .append(mainJarPath)
                    .append("\" %*");
        }
        builder.append("\nexit");
        return builder.toString();
    }
}
