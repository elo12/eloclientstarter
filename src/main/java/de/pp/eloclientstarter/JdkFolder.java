package de.pp.eloclientstarter;

import java.nio.file.Files;
import java.nio.file.Path;

public record JdkFolder(String name, Path location) {
    @Override
    public String toString() {
        return name;
    }

    public String getJavaExecutable() {
        if (isJavaExecutablePreset()) {
            return location.resolve("bin/java.exe").toAbsolutePath().toString();
        } else {
            return location.resolve("bin/ELOJavaClient.exe").toAbsolutePath().toString();
        }
    }

    private boolean isJavaExecutablePreset() {
        return Files.exists(location.resolve("bin/java.exe"));
    }
}