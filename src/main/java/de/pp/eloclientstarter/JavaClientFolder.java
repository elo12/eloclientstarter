package de.pp.eloclientstarter;

import java.nio.file.Files;
import java.nio.file.Path;

public record JavaClientFolder(String name, Path location) implements Comparable {

    @Override
    public String toString() {
        return this.name;
    }

    public String getLibraryPath() {
        return location.resolve("bin").toAbsolutePath().toString();
    }

    public String getJavaExecutable() {
        if (isJavaExecutablePreset()) {
            return location.resolve("jre/bin/java.exe").toAbsolutePath().toString();
        } else {
            return location.resolve("jre/bin/ELOJavaClient.exe").toAbsolutePath().toString();
        }
    }

    private boolean isJavaExecutablePreset() {
        return Files.exists(location.resolve("jre/bin/java.exe"));
    }

    public String getMainJarPath() {
        return location.resolve("EloClient.jar").toAbsolutePath().toString();
    }

    @Override
    public int compareTo(Object o) {
        JavaClientFolder other = (JavaClientFolder) o;
        JavaClientFolder that = this;
        return that.name().compareTo(other.name());
    }
}