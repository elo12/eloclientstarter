package de.pp.eloclientstarter;

import de.pp.eloclientstarter.settings.EloRegistry;
import de.pp.eloclientstarter.settings.Registry;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

public class MainFrame extends Application {

    private static MainFrame MainFrame;
    private Stage root;
    private Stage licence;

    private static final double PREF_WIDTH = 455;
    private static final double PREF_HEIGHT = 490;

    private static final double PREF_HEIGHT_BUTTONS = 415;

    private ListView<JavaClientFolder> listClients;
    private ListView<String> listArchives;
    private ListView<JdkFolder> listJDK;
    private ListView<String> listLogs;

    private TextField tfIx;
    private TextField tfAc;

    private CheckBox cbCustomJdk;

    private TextField tfDebugPort;
    private TextField tfMaxRam;


    public static void main(String[] args) {
        launch();
    }

    private void initData() {
        MainFrame = this;

        List<JavaClientFolder> javaClientFolderList;
        List<JdkFolder> jdkFolderList;
        var archiveNames = EloRegistry.getArchiveNames();
        javaClientFolderList = new ProcessDirectories().findClients();
        jdkFolderList = new ProcessDirectories().findJdks();

        var lastSelectedJavaClient = Registry.getLastSelectedJavaClient();
        addElements(javaClientFolderList, listClients, lastSelectedJavaClient);

        var lastSelectedJDK = Registry.getLastSelectedJDK();
        addElements(jdkFolderList, listJDK, lastSelectedJDK);

        var lastSelectedArchive = EloRegistry.getLastSelectedArchive();
        addElements(archiveNames, listArchives, lastSelectedArchive);
    }

    private void onClickStart(boolean debug) {
        var archive = listArchives.getSelectionModel().getSelectedItem();
        var jdk = listJDK.getSelectionModel().getSelectedItem();
        var javaClient = listClients.getSelectionModel().getSelectedItem();
        var customJdk = cbCustomJdk.isSelected();
        var debugPort = tfDebugPort.getText();
        var maxRam = tfMaxRam.getText();

        MainFrame.logInfo("Start ELO: archive=" + archive + " jdk=" + jdk + " javaClient=" + javaClient + " customJdk=" + customJdk);

        try {
            ClientRunner.startJC(archive, jdk, customJdk, javaClient, debug, debugPort, maxRam);
        } catch (IOException e) {
            MainFrame.logError("Start ELO : archive=" + archive + " jdk=" + jdk + " javaClient=" + javaClient + " customJdk=" + customJdk, e);
        }
    }

    private <T> void addElements(List<T> elements, ListView<T> listView, String selected) {
        for (T element : elements) {
            listView.getItems().add(element);
            if (element.toString().equals(selected)) {
                listView.getSelectionModel().select(element);
            }
        }
    }

    public static void logInfo(String message) {
        if (MainFrame != null) {
            MainFrame.listLogs.getItems().add("INFO: " + message);
        }
    }

    public static void logError(String message, Throwable throwable) {
        if (MainFrame != null) {
            MainFrame.listLogs.getItems().add("ERROR: " + message + " Exception=" + throwable.getMessage());
        }
    }

    @Override
    public void start(Stage primaryStage) {
        TabPane tabPane = new TabPane();

        tabPane.getTabs().add(makeGeneralTab());
        tabPane.getTabs().add(makeAdvancedTab());
        tabPane.getTabs().add(makeLoggingTab());

        tabPane.getSelectionModel().getSelectedItem();
        tabPane.setPrefWidth(PREF_WIDTH);
        tabPane.setPrefHeight(320);

        Button btStart = new Button("Start");
        btStart.setLayoutX(274);
        btStart.setLayoutY(PREF_HEIGHT_BUTTONS);
        btStart.setMinWidth(80);
        btStart.setStyle("-fx-background-color: #7cbd51;");
        btStart.setOnAction(event -> onClickStart(false));

        Button btDebug = new Button("Debug");
        btDebug.setLayoutX(361);
        btDebug.setLayoutY(PREF_HEIGHT_BUTTONS);
        btDebug.setMinWidth(80);
        btDebug.setStyle("-fx-background-color: #13b0f5;");
        btDebug.setOnAction(event -> onClickStart(true));

        CheckBox cbSSO = new CheckBox("SSO");
        cbSSO.setLayoutX(206);
        cbSSO.setLayoutY(PREF_HEIGHT_BUTTONS);
        cbSSO.setSelected("true".equals(EloRegistry.getSingleSignOn()));
        cbSSO.setTooltip(Util.makeTooltip("Single Sign On"));
        cbSSO.selectedProperty().addListener((observable, oldValue, newValue) -> EloRegistry.setSingleSignOn(newValue));

        TextField tfLastLogin = new TextField();
        tfLastLogin.setLayoutX(14);
        tfLastLogin.setLayoutY(PREF_HEIGHT_BUTTONS);
        tfLastLogin.setMinWidth(180);
        tfLastLogin.setText(EloRegistry.getLastLogin());
        tfLastLogin.setTooltip(Util.makeTooltip("Last login / Username"));
        tfLastLogin.textProperty().addListener((observable, oldValue, newValue) -> EloRegistry.setLastLogin(newValue));

        Label lbAc = new Label("AdminConsole");
        lbAc.setLayoutX(14);
        lbAc.setLayoutY(340);

        tfAc = new TextField("---");
        tfAc.setLayoutX(122);
        tfAc.setLayoutY(336);
        tfAc.setMinWidth(312);
        tfAc.setEditable(false);
        tfAc.setTooltip(Util.makeTooltip("Double-Click to open Url"));
        tfAc.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                if (mouseEvent.getClickCount() == 2) {
                    openUrl(tfAc.getText());
                }
            }
        });

        Label lbIx = new Label("IndexServer");
        lbIx.setLayoutX(14);
        lbIx.setLayoutY(370);

        tfIx = new TextField("---");
        tfIx.setLayoutX(122);
        tfIx.setLayoutY(368);
        tfIx.setMinWidth(312);
        tfIx.setEditable(false);
        tfIx.setTooltip(Util.makeTooltip("Double-Click to open Url"));
        tfIx.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                if (mouseEvent.getClickCount() == 2) {
                    openUrl(tfIx.getText());
                }
            }
        });

        Separator separator1 = new Separator();
        separator1.setLayoutX(0);
        separator1.setLayoutY(330);
        separator1.setMinWidth(PREF_WIDTH);

        Separator separator2 = new Separator();
        separator2.setLayoutX(0);
        separator2.setLayoutY(400);
        separator2.setMinWidth(PREF_WIDTH);

        AnchorPane anchorPane = new AnchorPane(tabPane, btStart, btDebug, cbSSO, tfLastLogin, separator1, separator2, lbAc, lbIx, tfAc, tfIx);
        anchorPane.setPrefHeight(455);
        anchorPane.setPrefWidth(PREF_WIDTH);

        root = createScene(primaryStage, anchorPane);

        initData();

        checkLicence();
        predictUrls();
    }

    private Stage createScene(Stage stage, AnchorPane anchorPane) {
        Scene scene = new Scene(anchorPane);
        stage.setScene(scene);
        stage.setTitle("ELO Client Starter");
        stage.setResizable(false);
        stage.show();
        return stage;
    }

    private Tab makeGeneralTab() {
        AnchorPane anchorPane = new AnchorPane();

        Label labelClients = new Label("Clients");
        labelClients.setLayoutX(14);
        labelClients.setLayoutY(14);
        anchorPane.getChildren().add(labelClients);

        Label labelRepositories = new Label("Repositories");
        labelRepositories.setLayoutX(235);
        labelRepositories.setLayoutY(14);
        anchorPane.getChildren().add(labelRepositories);

        listClients = new ListView<>();
        setDimensions(listClients, 14, 33, 200, 245);
        anchorPane.getChildren().add(listClients);

        listArchives = new ListView<>();
        listArchives.setTooltip(Util.makeTooltip("Double-Click to open"));
        listArchives.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            predictUrls();
        });
        listArchives.setOnMouseClicked(click -> {
            if (click.getClickCount() == 2) {
                onClickStart(false);
            }
        });
        setDimensions(listArchives, 235, 33, 200, 245);
        anchorPane.getChildren().add(listArchives);

        Tab tab = new Tab("General", anchorPane);
        tab.setClosable(false);
        return tab;
    }

    private Tab makeAdvancedTab() {
        AnchorPane anchorPane = new AnchorPane();

        Label lbJdk = new Label("JDK");
        lbJdk.setLayoutX(14);
        lbJdk.setLayoutY(14);
        anchorPane.getChildren().add(lbJdk);

        Label lbDebugPort = new Label("Debug Port");
        lbDebugPort.setLayoutX(228);
        lbDebugPort.setLayoutY(66);
        anchorPane.getChildren().add(lbDebugPort);

        Label lbMaxRam = new Label("Max RAM");
        lbMaxRam.setLayoutX(228);
        lbMaxRam.setLayoutY(114);
        anchorPane.getChildren().add(lbMaxRam);

        cbCustomJdk = new CheckBox("Custom JDK");
        cbCustomJdk.setLayoutX(228);
        cbCustomJdk.setLayoutY(33);
        cbCustomJdk.setSelected("true".equals(Registry.getLastCustomJDK()));
        cbCustomJdk.selectedProperty().addListener((observable, oldValue, newValue) -> Registry.setLastCustomJDK(newValue.toString()));
        anchorPane.getChildren().add(cbCustomJdk);

        tfDebugPort = new TextField("5007");
        tfDebugPort.setLayoutX(228);
        tfDebugPort.setLayoutY(83);
        tfDebugPort.setText(Registry.getLastSelectedDebugPort());
        anchorPane.getChildren().add(tfDebugPort);

        tfMaxRam = new TextField("1000");
        tfMaxRam.setLayoutX(228);
        tfMaxRam.setLayoutY(131);
        tfMaxRam.setText(Registry.getLastSelectedMaxRam());
        tfMaxRam.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) {
                tfMaxRam.setText("1000");
                return;
            }

            if (!Util.isInteger(newValue)) {
                tfMaxRam.setText(oldValue);
                return;
            }
            int maxRam = Integer.parseInt(tfMaxRam.getText());
            if (maxRam <= 0) {
                tfMaxRam.setText(oldValue);
                return;
            }

            Registry.setLastSelectedMaxRam(newValue);
        });
        anchorPane.getChildren().add(tfMaxRam);

        listJDK = new ListView<>();
        setDimensions(listJDK, 14, 33, 200, 245);
        anchorPane.getChildren().add(listJDK);

        Tab tab = new Tab("Advanced", anchorPane);
        tab.setClosable(false);
        return tab;
    }

    private Tab makeLoggingTab() {
        AnchorPane anchorPane = new AnchorPane();

        Label lbLogs = new Label("Logs");
        lbLogs.setLayoutX(14);
        lbLogs.setLayoutY(14);
        anchorPane.getChildren().add(lbLogs);

        listLogs = new ListView<>();
        setDimensions(listLogs, 14, 33, 427, 245);
        anchorPane.getChildren().add(listLogs);

        Tab tab = new Tab("Logs", anchorPane);
        tab.setClosable(false);
        return tab;
    }

    private void setDimensions(Region region, double x, double y, double width, double height) {
        region.setLayoutX(x);
        region.setLayoutY(y);
        region.setMaxWidth(width);
        region.setMinWidth(width);
        region.setMaxHeight(height);
        region.setMinHeight(height);
    }

    private void predictUrls() {
        var lastSelectedArchive = listArchives.getSelectionModel().getSelectedItem();
        if (lastSelectedArchive == null) {
            return;
        }

        String archiveUrl = EloRegistry.getArchiveUrl(lastSelectedArchive);


        var acUrl = getAcUrl(archiveUrl, false);

        doCheckUrl(tfIx, archiveUrl);
        doCheckUrl(tfAc, acUrl);

        System.err.println();
    }

    private String getAcUrl(String urlBase, boolean docker) {
        String servletEndpoint = "/ix";
        urlBase = urlBase.substring(0, urlBase.length() - servletEndpoint.length());

        if (docker) {
            return urlBase + "/plugin/de.elo.ix.plugin.proxy/administration/";
        } else {
            return urlBase + "/plugin/de.elo.ix.plugin.proxy/ac/";
        }
    }

    private void doCheckUrl(TextField tf, String url) {
        invokeLater(() -> {
            if (checkConnection(url)) {
                tf.setStyle("-fx-background-color: #b4ff99;");
            } else {
                tf.setStyle("-fx-background-color: #ff9191;");

                if (url.endsWith("/ac/")) {
                    String acUrl = url.replace("/ac/", "/administration/");
                    doCheckUrl(tf, acUrl);
                    return;
                }
            }

            tf.setText(url);
        });
    }

    private boolean checkConnection(String url) {
        HttpURLConnection connection;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(1000);
            int responseCode = connection.getResponseCode();
            if (responseCode != 200) {
                return false;
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    private void invokeLater(Runnable runnable) {
        new Thread(runnable).start();
    }

    private void openUrl(String url) {
        try {
            Desktop.getDesktop().browse(new URI(url));
        } catch (IOException | URISyntaxException e) {
            logError("Failed to open URL: " + url, e);
        }
    }

    private void checkLicence() {
        if (Registry.getLicenceAgreed().equals("true")) {
            MainFrame.logInfo("Licence already accepted");
        } else {
            MainFrame.logInfo("Licence not accepted. Open Licence Dialog");
            openCheckLicenceDialog();
            root.hide();
        }
    }

    private void onClickLicence(boolean accept) {
        Registry.setLicenceAgreed(String.valueOf(accept));
        licence.close();

        if (accept) {
            MainFrame.logInfo("Licence accepted. Show root");
            root.show();
        } else {
            MainFrame.logInfo("Licence declined. Close root");
            root.close();
        }
    }

    private void openCheckLicenceDialog() {
        var btAccept = new Button("Accept");
        btAccept.setLayoutX(259);
        btAccept.setLayoutY(349);
        btAccept.setMinWidth(80);
        btAccept.setStyle("-fx-background-color: #7cbd51;");
        btAccept.setOnAction(event -> onClickLicence(true));

        var btDecline = new Button("Decline");
        btDecline.setLayoutX(354);
        btDecline.setLayoutY(349);
        btDecline.setMinWidth(80);
        btDecline.setStyle("-fx-background-color: #fc3665;");
        btDecline.setOnAction(event -> onClickLicence(false));

        Separator separator = new Separator();
        separator.setLayoutX(0);
        separator.setLayoutY(330);
        separator.setMinWidth(PREF_WIDTH);

        Label label = new Label();
        label.setLayoutX(14);
        label.setLayoutY(14);

        label.setText("Copyright 2022 - 2023: Phillip Partsch <info@phillipppartsch.de>\n\n" +
                "THIS IS NOT A PRODUCT OF ELO DIGITAL OFFICE GMBH\n\n\n" +
                "Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "you may not use this file except in compliance with the License.\n" +
                "You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "Unless required by applicable law or agreed to in writing, software\n" +
                "distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "See the License for the specific language governing permissions and\n" +
                "limitations under the License.");

        AnchorPane anchorPane = new AnchorPane(btAccept, btDecline, separator, label);
        anchorPane.setPrefHeight(PREF_HEIGHT);
        anchorPane.setPrefWidth(PREF_WIDTH);

        licence = new Stage();
        licence.setTitle("License");
        licence.setScene(new Scene(anchorPane));
        licence.setResizable(false);
        licence.show();
    }
}
